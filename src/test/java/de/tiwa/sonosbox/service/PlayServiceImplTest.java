package de.tiwa.sonosbox.service;

import ch.qos.logback.classic.spi.LoggingEvent;
import de.tiwa.sonosbox.config.PlayConfig;
import de.tiwa.sonosbox.helper.SonosClientWrapper;
import de.tiwa.sonosbox.repository.ConfigurationRepository;
import de.tiwa.sonosbox.repository.PlayableItem;
import de.tiwa.sonosbox.repository.PlayableItemRepository;
import de.tiwa.sonosbox.repository.SonosUser;
import de.tiwa.sonosbox.testhelper.LogCapture;
import de.tiwa.sonosbox.testhelper.LogCaptureExtension;
import engineer.nightowl.sonos.api.domain.*;
import engineer.nightowl.sonos.api.exception.SonosApiClientException;
import engineer.nightowl.sonos.api.exception.SonosApiError;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(LogCaptureExtension.class)
@ExtendWith(MockitoExtension.class)
class PlayServiceImplTest {

    private PlayService sut;
    @Mock
    private ConfigurationRepository configurationRepository;
    @Mock
    private PlayableItemRepository playableItemRepositoryMock;

    private PlayConfig playConfig;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private SonosClientWrapper clientMock;

    @BeforeEach
    void setUp() {
        playConfig = new PlayConfig("room");
        sut = new PlayServiceImpl(configurationRepository, clientMock, playableItemRepositoryMock, playConfig);
    }

    @Test
    void markerNotInRepository_createNewPlayableItem(LogCapture logCapture) {
        // prepare
        String marker = "marker";
        when(playableItemRepositoryMock.findByMarkerId(marker)).thenReturn(Optional.empty());

        // execute
        sut.play(marker);

        // verify
        verify(playableItemRepositoryMock).save(any());

        List<LoggingEvent> loggingEvents = logCapture.getLoggingEvents();
        Assertions.assertThat(loggingEvents.get(0).getFormattedMessage()).isEqualTo("Play: marker");
        Assertions.assertThat(loggingEvents.get(1).getFormattedMessage()).isEqualTo("playable item not found for: marker");
    }

    @Test
    void markerInRepositoryWithoutTrackAssigned_doNothing(LogCapture logCapture) {
        // prepare
        String marker = "marker";
        when(playableItemRepositoryMock.findByMarkerId(marker)).thenReturn(Optional.of(new PlayableItem()));

        // execute
        sut.play(marker);

        // verify
        List<LoggingEvent> loggingEvents = logCapture.getLoggingEvents();
        Assertions.assertThat(loggingEvents.get(0).getFormattedMessage()).isEqualTo("Play: marker");
        Assertions.assertThat(loggingEvents.get(1).getFormattedMessage()).isEqualTo("Track not found for: marker");
    }

    @Test
    void multipleHouseholds_Exception(LogCapture logCapture) throws SonosApiError, SonosApiClientException {
        // prepare
        String marker = "marker";
        String track = "track";
        PlayableItem playableItem = new PlayableItem();
        playableItem.setMarkerId(marker);
        playableItem.setTrack(track);
        when(playableItemRepositoryMock.findByMarkerId(marker)).thenReturn(Optional.of(playableItem));
        SonosUser sonosUser = new SonosUser("token", "state");
        when(configurationRepository.get(eq(SonosUser.class))).thenReturn(sonosUser);

        SonosHouseholdList sonosHouseholdList = new SonosHouseholdList(List.of(new SonosHousehold(), new SonosHousehold()));
        when(clientMock.getClient().household().getHouseholds(eq(sonosUser.getToken()))).thenReturn(sonosHouseholdList);

        // execute
        org.junit.jupiter.api.Assertions.assertThrows(IllegalStateException.class, () -> {
            sut.play(marker);
        });

        // verify
        List<LoggingEvent> loggingEvents = logCapture.getLoggingEvents();
        Assertions.assertThat(loggingEvents.get(1).getFormattedMessage()).isEqualTo("More then one household");
    }

    @Test
    void noFavouriteWithTrackName_Exception(LogCapture logCapture) throws SonosApiError, SonosApiClientException {
        // prepare
        String marker = "marker";
        String track = "track";
        PlayableItem playableItem = new PlayableItem();
        playableItem.setMarkerId(marker);
        playableItem.setTrack(track);
        when(playableItemRepositoryMock.findByMarkerId(marker)).thenReturn(Optional.of(playableItem));
        SonosUser sonosUser = new SonosUser("token", "state");
        when(configurationRepository.get(eq(SonosUser.class))).thenReturn(sonosUser);

        SonosHouseholdList sonosHouseholdList = new SonosHouseholdList(List.of(new SonosHousehold()));
        when(clientMock.getClient().household().getHouseholds(eq(sonosUser.getToken()))).thenReturn(sonosHouseholdList);

        List<SonosFavorite> sonosFavorites = new ArrayList<>();
        sonosFavorites.add(new SonosFavorite(null, "track", null, null, null, null, null));
        sonosFavorites.add(new SonosFavorite(null, "track2", null, null, null, null, null));
        sonosFavorites.add(new SonosFavorite(null, "track3", null, null, null, null, null));
        when(clientMock.getClient().favorite().getFavorites(any(), any())).thenReturn(new SonosFavoriteList("1", sonosFavorites));
        // execute
        org.junit.jupiter.api.Assertions.assertThrows(IllegalStateException.class, () -> {
            sut.play(marker);
        });

        // verify
        List<LoggingEvent> loggingEvents = logCapture.getLoggingEvents();
        Assertions.assertThat(loggingEvents.get(1).getFormattedMessage()).isEqualTo("Not a know speaker: room");
    }

    @Test
    void speakerNotKnown_Exception(LogCapture logCapture) throws SonosApiError, SonosApiClientException {
        // prepare
        String marker = "marker";
        String track = "track";
        PlayableItem playableItem = new PlayableItem();
        playableItem.setMarkerId(marker);
        playableItem.setTrack(track);
        when(playableItemRepositoryMock.findByMarkerId(marker)).thenReturn(Optional.of(playableItem));
        SonosUser sonosUser = new SonosUser("token", "state");
        when(configurationRepository.get(eq(SonosUser.class))).thenReturn(sonosUser);

        SonosHouseholdList sonosHouseholdList = new SonosHouseholdList(List.of(new SonosHousehold()));
        when(clientMock.getClient().household().getHouseholds(eq(sonosUser.getToken()))).thenReturn(sonosHouseholdList);

        // execute
        org.junit.jupiter.api.Assertions.assertThrows(IllegalStateException.class, () -> {
            sut.play(marker);
        });

        // verify
        List<LoggingEvent> loggingEvents = logCapture.getLoggingEvents();
        Assertions.assertThat(loggingEvents.get(1).getFormattedMessage()).isEqualTo("Not a Sonos favourite: track");
    }

    @Test
    @Disabled
    void happyPath_noException(LogCapture logCapture) throws SonosApiError, SonosApiClientException {
        // prepare
        String marker = "marker";
        String track = "track";
        PlayableItem playableItem = new PlayableItem();
        playableItem.setMarkerId(marker);
        playableItem.setTrack(track);
        when(playableItemRepositoryMock.findByMarkerId(marker)).thenReturn(Optional.of(playableItem));
        SonosUser sonosUser = new SonosUser("token", "state");
        when(configurationRepository.get(eq(SonosUser.class))).thenReturn(sonosUser);

        SonosHouseholdList sonosHouseholdList = new SonosHouseholdList(List.of(new SonosHousehold()));
        when(clientMock.getClient().household().getHouseholds(eq(sonosUser.getToken()))).thenReturn(sonosHouseholdList);

        List<SonosFavorite> sonosFavorites = new ArrayList<>();
        sonosFavorites.add(new SonosFavorite(null, "track", null, null, null, null, null));
        sonosFavorites.add(new SonosFavorite(null, "track2", null, null, null, null, null));
        sonosFavorites.add(new SonosFavorite(null, "track3", null, null, null, null, null));
        when(clientMock.getClient().favorite().getFavorites(any(), any())).thenReturn(new SonosFavoriteList("1", sonosFavorites));

        SonosPlayer sonosPlayer = new SonosPlayer(null, null, null, "id", null, "room", null, null, null, null);
        SonosGroups sonosGroups = new SonosGroups(null, List.of(sonosPlayer));
        when(clientMock.getClient().group().getGroups(any(), any())).thenReturn(sonosGroups);
        when(clientMock.getClient().group().createGroup(eq(sonosUser.getToken()), any(),
                Mockito.anyList(), eq(null))).thenReturn(new SonosGroupInfo());
        // execute
        sut.play(marker);

        // verify
    }

}