package de.tiwa.sonosbox.controller;

import de.tiwa.sonosbox.service.PlayService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class PlayControllerTest {

   private PlayController sut;

   private PlayService playServiceMock;

    @BeforeEach
    void setUp() {
        playServiceMock = Mockito.mock(PlayService.class);
        // spring mvc todo
        sut = new PlayController(playServiceMock);
    }

    @Test
    public void play() {
        // prepare
        String marker = "marker";
        // execute
        sut.play(marker);
        // verify
        Mockito.verify(playServiceMock).play(Mockito.eq(marker));

    }

}