package de.tiwa.sonosbox.repository;

import de.tiwa.sonosbox.config.M2Config;
import lombok.Data;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConfigurationRepositoryTest {

    ConfigurationRepository sut;

    @BeforeEach
    public void setUp() {
        M2Config m2Config = new M2Config(null);
        sut = new ConfigurationRepository(m2Config);
    }

    @Test
    public void persistAndGetObject() {
        // prepare
        SonosUser sonosUser = new SonosUser();
        sonosUser.setState("state");
        sonosUser.setToken("token123");

        // execute
        sut.persist(sonosUser);
        SonosUser sonosUser1 = sut.get(SonosUser.class);

        // verify
        Assertions.assertThat(sonosUser1.getToken()).isEqualTo(sonosUser.getToken());
        Assertions.assertThat(sonosUser1.getState()).isEqualTo(sonosUser.getState());
    }

    @Test
    public void persistObjectWithNonStringField_IllegalArgumentException() {
        // prepare
        TestClass testClass = new TestClass();
        testClass.setId(123);
        testClass.setTestString("String");

        // execute
        IllegalArgumentException thrown = org.junit.jupiter.api.Assertions.assertThrows(IllegalArgumentException.class, () -> {
            sut.persist(testClass);
        });

        // verify
        Assertions.assertThat(thrown.getMessage()).isEqualTo("Only String fields supported.");
    }

    @Data
    public class TestClass {
        private int id;

        private String testString;
    }
}