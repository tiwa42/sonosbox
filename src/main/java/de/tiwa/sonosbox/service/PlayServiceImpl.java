package de.tiwa.sonosbox.service;

import de.tiwa.sonosbox.config.PlayConfig;
import de.tiwa.sonosbox.helper.SonosClientWrapper;
import de.tiwa.sonosbox.repository.ConfigurationRepository;
import de.tiwa.sonosbox.repository.PlayableItem;
import de.tiwa.sonosbox.repository.PlayableItemRepository;
import de.tiwa.sonosbox.repository.SonosUser;
import engineer.nightowl.sonos.api.domain.*;
import engineer.nightowl.sonos.api.exception.SonosApiClientException;
import engineer.nightowl.sonos.api.exception.SonosApiError;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class PlayServiceImpl implements PlayService {

    private Logger logger = LoggerFactory.getLogger(PlayServiceImpl.class);

    private final ConfigurationRepository configurationRepository;

    private final SonosClientWrapper clientWrapper;

    private final PlayableItemRepository playableItemRepository;

    private final PlayConfig playConfig;

    public PlayServiceImpl(ConfigurationRepository configurationRepository, SonosClientWrapper clientWrapper,
                           PlayableItemRepository playableItemRepository, PlayConfig playConfig) {
        this.configurationRepository = configurationRepository;
        this.clientWrapper = clientWrapper;
        this.playableItemRepository = playableItemRepository;
        this.playConfig = playConfig;

    }

    @Override
    public void play(String marker) {
        logger.info("Play: " + marker);
        Optional<PlayableItem> playableItemOptional = playableItemRepository.findByMarkerId(marker);
        if (playableItemOptional.isEmpty()) {
            logger.info("playable item not found for: " + marker);
            createPlayableItem(marker);
        } else {
            PlayableItem playableItem = playableItemOptional.get();
            if (StringUtils.isEmpty(playableItem.getTrack())) {
                logger.warn("Track not found for: " + marker);
                return;
            }
            playTrack(playableItem);
        }
    }

    private void createPlayableItem(String marker) {
        PlayableItem playableItemNew = new PlayableItem();
        playableItemNew.setMarkerId(marker);
        playableItemRepository.save(playableItemNew);
    }

    private void playTrack(PlayableItem playableItem) {
        SonosUser sonosUser = configurationRepository.get(SonosUser.class);
        try {
            SonosHouseholdList households = clientWrapper.getClient().household().getHouseholds(sonosUser.getToken());
            if (households.getHouseholds().size() != 1) {
                logger.error("More then one household");
                throw new IllegalStateException();
            }
            SonosHousehold sonosHousehold = households.getHouseholds().get(0);
            List<SonosFavorite> sonosFavorites = clientWrapper.getClient().favorite().getFavorites(sonosUser.getToken(), sonosHousehold.getId()).getItems();
            Optional<SonosFavorite> sonosFavorite = sonosFavorites.stream().filter(sf -> sf.getName().equals(playableItem.getTrack())).findFirst();
            if (sonosFavorite.isEmpty()) {
                logger.warn("Not a Sonos favourite: " + playableItem.getTrack());
                throw new IllegalStateException();
            }
            SonosGroups groups = clientWrapper.getClient().group().getGroups(sonosUser.getToken(), sonosHousehold.getId());
            Optional<SonosPlayer> sonosPlayer = groups.getPlayers().stream().filter(sp -> sp.getName().equals(playConfig.room())).findFirst();
            if (sonosPlayer.isEmpty()) {
                logger.error("Not a know speaker: " + playConfig.room());
                throw new IllegalStateException();
            }
            SonosGroupInfo group = clientWrapper.getClient().group().createGroup(sonosUser.getToken(), sonosHousehold.getId(),
                    List.of(sonosPlayer.get().getId()), null);
            clientWrapper.getClient().favorite().loadFavorite(sonosUser.getToken(), group.getGroup().getId(),sonosFavorite.get().getId(),true,null);
        } catch (SonosApiClientException e) {
            throw new RuntimeException(e);
        } catch (SonosApiError e) {
            throw new RuntimeException(e);
        }
    }
}
