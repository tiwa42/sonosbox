package de.tiwa.sonosbox.service;

import de.tiwa.sonosbox.config.SonosCredentialsConfig;
import de.tiwa.sonosbox.repository.ConfigurationRepository;
import de.tiwa.sonosbox.repository.SonosUser;
import engineer.nightowl.sonos.api.SonosApiClient;
import engineer.nightowl.sonos.api.SonosApiConfiguration;
import engineer.nightowl.sonos.api.domain.SonosToken;
import engineer.nightowl.sonos.api.exception.SonosApiClientException;
import engineer.nightowl.sonos.api.exception.SonosApiError;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.net.URISyntaxException;


@Service
public class SonosUserServiceImpl implements SonosUserService {


    private final ConfigurationRepository configurationRepository;
    private static final String REDIRECT_URI = "http://localhost:8080";
    private final SonosApiClient client;

    public SonosUserServiceImpl(SonosCredentialsConfig sonosCredentialsConfig, ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
        final SonosApiConfiguration configuration = new SonosApiConfiguration();
        configuration.setApiKey(sonosCredentialsConfig.apiKey());
        configuration.setApiSecret(sonosCredentialsConfig.apiSecret());
        configuration.setApplicationId(sonosCredentialsConfig.applicationId());
        client = new SonosApiClient(configuration);
    }

    public URI getAuthorizeCodeUri() {
        try {
            String stateValue = client.authorize().generateStateValue();
            SonosUser sonosUser = new SonosUser();
            sonosUser.setState(stateValue);
            configurationRepository.persist(sonosUser);

            return client.authorize().getAuthorizeCodeUri(REDIRECT_URI + "/setAuthorizeCode", stateValue);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public void setAuthorizeCode(String state, String authorizeCode) {
        try {
            SonosUser sonosUser = configurationRepository.get(SonosUser.class);
            SonosToken token = client.authorize().createToken(REDIRECT_URI + "/setAuthorizeCode", authorizeCode);
            sonosUser.setToken(token.getAccessToken());
            // refresh token etc todo
            configurationRepository.persist(sonosUser);
        } catch (SonosApiClientException e) {
            throw new RuntimeException(e);
        } catch (SonosApiError e) {
            throw new RuntimeException(e);
        }
    }
}
