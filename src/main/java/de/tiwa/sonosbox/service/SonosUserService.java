package de.tiwa.sonosbox.service;

import java.net.URI;

public interface SonosUserService {
     URI getAuthorizeCodeUri();

     void setAuthorizeCode(String state, String authorizeCode);
}
