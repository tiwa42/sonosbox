package de.tiwa.sonosbox.helper;

import de.tiwa.sonosbox.config.SonosCredentialsConfig;
import engineer.nightowl.sonos.api.SonosApiClient;
import engineer.nightowl.sonos.api.SonosApiConfiguration;
import org.springframework.stereotype.Component;

@Component
public class SonosClientWrapper {

    private final SonosApiClient client;

    public SonosClientWrapper(SonosCredentialsConfig sonosCredentialsConfig) {
        final SonosApiConfiguration configuration = new SonosApiConfiguration();
        configuration.setApiKey(sonosCredentialsConfig.apiKey());
        configuration.setApiSecret(sonosCredentialsConfig.apiSecret());
        configuration.setApplicationId(sonosCredentialsConfig.applicationId());
        this.client = new SonosApiClient(configuration);
    }

    public SonosApiClient getClient() {
        return client;
    }
}
