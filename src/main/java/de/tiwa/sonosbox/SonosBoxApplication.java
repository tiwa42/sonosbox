package de.tiwa.sonosbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ConfigurationPropertiesScan
public class SonosBoxApplication {

	public static void main(String[] args) {
		SpringApplication.run(SonosBoxApplication.class, args);
	}

}
