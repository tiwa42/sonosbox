package de.tiwa.sonosbox.repository;

import de.tiwa.sonosbox.config.M2Config;
import org.h2.mvstore.MVMap;
import org.h2.mvstore.MVStore;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;

@Component
public class ConfigurationRepository {

    private final MVStore s;

    public ConfigurationRepository(M2Config m2Config) {
        this.s = MVStore.open(m2Config.file());
    }

    public void persist(Object object) {
        Method[] declaredMethods = ReflectionUtils.getDeclaredMethods(object.getClass());
        HashSet<Method> methods = Arrays.stream(declaredMethods).filter(method -> method.getName().startsWith("get")).collect(Collectors.toCollection(HashSet::new));
        MVMap<String, String> map = s.openMap(object.getClass().getName());
        map.clear();
        for (Method method : methods) {
            Class<?> returnType = method.getReturnType();
            if (returnType != String.class) {
                throw new IllegalArgumentException("Only String fields supported.");
            }
            String key = method.getName().substring(3);
            String value = (String) ReflectionUtils.invokeMethod(method, object);
            map.put(key, value);
        }
    }

    public <T> T get(Class<T> aClass) {
        MVMap<Object, Object> objectObjectMVMap = s.openMap(aClass.getName());
        try {
            T target = aClass.getDeclaredConstructor().newInstance();
            Method[] declaredMethods = ReflectionUtils.getDeclaredMethods(aClass);
            HashSet<Method> methods = Arrays.stream(declaredMethods).filter(method -> method.getName().startsWith("set")).collect(Collectors.toCollection(HashSet::new));
            for (Method method : methods) {
                ReflectionUtils.invokeMethod(method, target, objectObjectMVMap.get(method.getName().substring(3)));
            }
            return target;
        } catch (Exception e) {
            new RuntimeException(e);
        }
        throw new IllegalStateException();
    }
}
