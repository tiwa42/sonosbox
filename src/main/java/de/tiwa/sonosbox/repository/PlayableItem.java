package de.tiwa.sonosbox.repository;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Table
@Data
public class PlayableItem {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(unique = true)
    private String markerId;

    @Column
    private String track;
}
