package de.tiwa.sonosbox.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlayableItemRepository extends CrudRepository<PlayableItem, Integer> {
    Optional<PlayableItem> findByMarkerId(String Marker);
}
