package de.tiwa.sonosbox.repository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SonosUser {
    private String token;

    private String state;
}
