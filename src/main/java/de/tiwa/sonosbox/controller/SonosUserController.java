package de.tiwa.sonosbox.controller;

import de.tiwa.sonosbox.service.SonosUserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import java.net.MalformedURLException;
@RestController()
@RequestMapping("/user")
public class SonosUserController {

    private final SonosUserService sonosUserService;

    public SonosUserController(SonosUserService sonosUserService) {
        this.sonosUserService = sonosUserService;
    }

    @GetMapping("/loginToSonos")
    public RedirectView loginToSonos() {
        try {
            return new RedirectView(sonosUserService.getAuthorizeCodeUri().toURL().toString());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/setAuthorizeCode")
    public RedirectView setAuthorizeCode(@RequestParam("state") String state, @RequestParam("code") String authorizeCode) {
        sonosUserService.setAuthorizeCode(state, authorizeCode);
        return new RedirectView("www.seitenbau.com");
    }
}
