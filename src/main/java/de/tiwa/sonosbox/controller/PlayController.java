package de.tiwa.sonosbox.controller;

import de.tiwa.sonosbox.service.PlayService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
public class PlayController {

    private final PlayService playService;

    public PlayController(PlayService playService) {
        this.playService = playService;
    }

    @GetMapping("/play")
    public void play(@RequestParam("marker")String marker) {
        playService.play(marker);
    }
}
