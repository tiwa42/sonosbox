package de.tiwa.sonosbox.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@ConfigurationProperties(prefix = "sonos.credentials")
public record SonosCredentialsConfig(String apiKey, String apiSecret, String applicationId) {
}
