package de.tiwa.sonosbox.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "play")
public record PlayConfig(String room) {
}