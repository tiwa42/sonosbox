package de.tiwa.sonosbox.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "play.datasource")
public record M2Config(String file) {
}
